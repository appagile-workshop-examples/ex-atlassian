FROM openshift/base-centos7

LABEL vendor="T-Systems AppAgile" \
      author="C.Wohlan" \
      release="2018-04-09"

### OpenJDK 1.8
RUN yum update -y --setopt=tsflags=nodocs \
 && yum install -y --setopt=tsflags=nodocs wget perl \
 && yum install -y --setopt=tsflags=nodocs java-1.8.0-openjdk-devel \
 && rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
 && yum install -y --setopt=tsflags=nodocs xmlstarlet \
 && rpm -e $(rpm -qa | grep -i epel-release) \
 && yum clean all \
 && rm -rf /var/cache/yum

ENV JAVA_HOME /usr/lib/jvm/java-1.8.0-openjdk
ENV PATH $JAVA_HOME/bin:$PATH
ENV LANG en_US.UTF-8
RUN localedef -i en_US -f UTF-8 en_US.UTF-8

### Confluence
ARG CONFLUENCE_VERSION=6.3.3
ARG DOWNLOAD_URL=http://www.atlassian.com/software/confluence/downloads/binary/atlassian-confluence-${CONFLUENCE_VERSION}.tar.gz

LABEL io.k8s.description="Atlassian Confluence container " \
      io.k8s.display-name="Atlassian Confluence ${CONFLUENCE_VERSION}" \
      io.openshift.tags="config,atlassian,confluence" \
      io.openshift.non-scalable="true"

ENV CONFLUENCE_HOME=/var/atlassian/application-data/confluence
ENV CONFLUENCE_INSTALL=/opt/atlassian/confluence

RUN mkdir -p "${CONFLUENCE_HOME}" \ 
 && chmod -R 770 "${CONFLUENCE_HOME}" \
 && chgrp -R 0 "${CONFLUENCE_HOME}" \
 && mkdir -p "${CONFLUENCE_INSTALL}" \
 && curl -Ls "${DOWNLOAD_URL}" | tar -xz --directory "${CONFLUENCE_INSTALL}" --strip-components=1 --no-same-owner \
 && sed -i -e 's/-Xms\([0-9]\+[kmg]\) -Xmx\([0-9]\+[kmg]\)/-Xms\${JVM_MINIMUM_MEMORY:=\1} -Xmx\${JVM_MAXIMUM_MEMORY:=\2} \${JVM_SUPPORT_RECOMMENDED_ARGS} -Dconfluence.home=\${CONFLUENCE_HOME}/g' ${CONFLUENCE_INSTALL}/bin/setenv.sh \
 && chmod -R 775 "${CONFLUENCE_INSTALL}" \
 && chgrp -R   0 "${CONFLUENCE_INSTALL}"

COPY entrypoint.sh  /entrypoint.sh
RUN chmod 755 /entrypoint.sh

VOLUME ["${CONFLUENCE_HOME}"]
WORKDIR "$CONFLUENCE_HOME"
EXPOSE 8090

USER 1000
ENTRYPOINT ["/entrypoint.sh"]
