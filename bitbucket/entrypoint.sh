#!/bin/bash
set -euo pipefail

ARGS="$@"

# Start Bitbucket without Elasticsearch
ARGS="--no-search ${ARGS}"

export JVM_SUPPORT_RECOMMENDED_ARGS="-Dcluster.node.name=$HOSTNAME"
if [ -f ${BITBUCKET_HOME}/shared/cacerts ]; then 
    JVM_SUPPORT_RECOMMENDED_ARGS="${JVM_SUPPORT_RECOMMENDED_ARGS} -Djavax.net.ssl.trustStore=${BITBUCKET_HOME}/shared/cacerts"
fi

# Set xms and xmx for JVM
BYTES_PER_MEG=$((1024*1024))
BYTES_PER_GIG=$((1024*${BYTES_PER_MEG}))

MIN_MEMORY_BYTES=$((384*${BYTES_PER_MEG}))

regex='^([[:digit:]]+)([GgMm])i?$'
if [[ "${HEAP_SIZE:-}" =~ $regex ]]; then
    echo "Setting Xmx and Xms memory based on provided HEAP_SIZE environment variable value: ${HEAP_SIZE}"
    num=${BASH_REMATCH[1]}
    unit=${BASH_REMATCH[2]}
    if [[ $unit =~ [Gg] ]]; then
        ((num = num * ${BYTES_PER_GIG})) # enables math to work out for odd Gi
    elif [[ $unit =~ [Mm] ]]; then
        ((num = num * ${BYTES_PER_MEG})) # enables math to work out for odd Mi
    fi

	echo "Inspecting the maximum RAM available..."

    container_mem_file="/sys/fs/cgroup/memory/memory.limit_in_bytes"
    node_mem_file="/proc/meminfo"
    if [ -r "${container_mem_file}" ] && [ -r "${node_mem_file}" ]; then
        container_mem=$(cat $container_mem_file)
        node_mem=$(cat $node_mem_file | grep MemAvailable | awk '{print $2 * 1024}')
        if [ ${container_mem} -lt ${node_mem} ]; then
            avail_mem=${container_mem}
        else
            avail_mem=${node_mem}
        fi

        if [ ${avail_mem} -lt ${num} ]; then
            ((num = ${avail_mem}))
            echo "Setting the heap size to $(($num / BYTES_PER_MEG))m which is the largest amount available in this container."
        fi
    else
        echo "Unable to determine the maximum allowable RAM for this container's cgroup or node."
        exit 1
    fi

    if [[ $num -lt $MIN_MEMORY_BYTES ]]; then
        echo "A minimum of $(($MIN_MEMORY_BYTES / $BYTES_PER_MEG))m heap size is required but only $(($num / $BYTES_PER_MEG))m is available or was specified."
        exit 1
    fi

    echo "The heap size will now be set to $(($num * 100 / avail_mem))% of the available memory."

	# set value in setenv.sh
    cp ${BITBUCKET_INSTALL}/bin/setenv.sh /tmp/setenv.sh
 	sed -i s/JVM_MINIMUM_MEMORY=.*/JVM_MINIMUM_MEMORY=\""${num}"\"/ /tmp/setenv.sh
 	sed -i s/JVM_MAXIMUM_MEMORY=.*/JVM_MAXIMUM_MEMORY=\""${num}"\"/ /tmp/setenv.sh
 	cat /tmp/setenv.sh > ${BITBUCKET_INSTALL}/bin/setenv.sh
else
    echo "HEAP_SIZE environment variable not set or invalid: ${HEAP_SIZE:-}"
    echo "Keeping default standard values. Set HEAP_SIZE for increased performance."
fi

# start Bitbucket (with -fg = forground)
exec "$BITBUCKET_INSTALL/bin/start-bitbucket.sh" -fg ${ARGS}